import { shallowMount } from '@vue/test-utils';
import App from '@/App.vue';

describe('App', () => {
  // Now mount the component and you have the wrapper
  const wrapper = shallowMount(App);

  it('renders the correct markup', () => {
    expect(wrapper.html()).toContain('<span class="count">0</span>')
  });

  // it's also easy to check for the existence of elements
  it('has a button', () => {
    expect(wrapper.contains('button')).toBe(true)
  });

  it('button click should increment the count', () => {
    expect(wrapper.vm.count).toBe(0);
    const increase_button = wrapper.find('#increase_button');
    increase_button.trigger('click');
    expect(wrapper.vm.count).toBe(1);
  });

  it('button click should decrement the count', () => {
    expect(wrapper.vm.count).toBe(1);
    const decrease_button = wrapper.find('#decrease_button');
    decrease_button.trigger('click');
    expect(wrapper.vm.count).toBe(0);
  });

});
