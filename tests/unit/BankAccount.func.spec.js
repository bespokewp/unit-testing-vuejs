import { shallowMount } from '@vue/test-utils';
import BankAccount from '@/components/BankAccount.vue';
import store from '@/store.vuex'; // component still controls this

describe('BankAccount', () => {
  // Now mount the component and you have the wrapper
  const wrapper = shallowMount(BankAccount);
  let original_value;
  let submit_button;
  let amount_input;
  let deduct_button;

  // set up for each of the tests, making sure reused variables are always available
  beforeEach(() => {
    // hold the original value to compare the modified results
    original_value = store.state.amount;

    // event triggers and modifiers
    submit_button = wrapper.find('#addAmountButton');
    amount_input = wrapper.find('#toModifyContainer');
    deduct_button = wrapper.find('#deductAmountButton');
  });

  it('adding 500 to fresh account makes a balance of 500', () => {
    amount_input.setValue(500);
    submit_button.trigger('click');
    expect(store.state.amount).toBe(original_value + 500);

  });

  it('adding a string of 500 to existing account of 500 (int) makes 1000', () => {
    amount_input.setValue('500');
    submit_button.trigger('click');
    expect(store.state.amount).toBe(original_value + 500);
  });

  it('passing in an invalid value (negative or not a number) should not affect the amount', () => {
    // negative number are invalid
    amount_input.setValue('-500');
    submit_button.trigger('click');
    expect(store.state.amount).toBe(original_value);

    // make sure negative int types are also caught
    amount_input.setValue(-500);
    submit_button.trigger('click');
    expect(store.state.amount).toBe(original_value);

    // amount must be a number
    amount_input.setValue('cheese');
    submit_button.trigger('click');
    expect(store.state.amount).toBe(original_value);
  });

  it('hitting deduct button removes amount from account', () => {
    amount_input.setValue('500');
    deduct_button.trigger('click');
    expect(store.state.amount).toBe(original_value - 500);
  });

  it('apply fee to the account if account becomes overdrawn', () => {
    const overdrawn_amount = 200;
    amount_input.setValue(original_value + overdrawn_amount);
    deduct_button.trigger('click');
    expect(store.state.amount).toBe(-(overdrawn_amount+5));
  });


});
