import { shallowMount } from '@vue/test-utils';
import BankAccount from '@/components/BankAccount.vue';
import store from '@/store.vuex'; // component still controls this

describe('BankAccount', () => {
  // Now mount the component and you have the wrapper
  const wrapper = shallowMount(BankAccount);
  let original_value;
  let submit_button;
  let amount_input;
  let deduct_button;
  let notice_container;
  let status_container;

  // set up for each of the tests, making sure reused variables are always available
  beforeEach(() => {
    // hold the original value to compare the modified results
    original_value = store.state.amount;

    // event triggers and modifiers
    submit_button = wrapper.find('#addAmountButton');
    amount_input = wrapper.find('#toModifyContainer');
    deduct_button = wrapper.find('#deductAmountButton');
    notice_container = wrapper.find('#notice_container');
    status_container = wrapper.find('#status_container');
  });

  it('does UI show that the new amount was added successfully?', () => {
    amount_input.setValue(500);
    submit_button.trigger('click');

    // was 500 added?
    expect(store.state.amount).toBe(original_value + 500);

    // does the UI show the updated state change?
    expect(status_container.text().toLowerCase()).toContain('updated');

    amount_input.setValue('cheese');
    deduct_button.trigger('click');
    // amount should not have changed from last update
    expect(store.state.amount).toBe(original_value + 500);

    // does the UI show the update failed?
    expect(status_container.text().toLowerCase()).toContain('update failed');
  });

  it('does UI show that the new amount was increased/decreased?', () => {
    // add an amount
    amount_input.setValue(500);
    submit_button.trigger('click');
    expect(store.state.amount).toBe(original_value + 500);
    expect(notice_container.text().toLowerCase()).toContain('increased');

    // deduct an amount
    amount_input.setValue(500);
    deduct_button.trigger('click');
    expect(store.state.amount).toBe(original_value);
    expect(notice_container.text().toLowerCase()).toContain('decreased');
  });



});
