import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);


export default new Vuex.Store({
  state: {
    count: 0,
    amount: 0
  },
  mutations: {
    increment (state) {
      state.count++
    },
    decrement (state) {
      state.count--
    },
    addFunds (state, payload) {

      // check to see if this is a valid transaction
      if ( !isNaN( payload.to_add ) && payload.to_add > 0 )
        state.amount += parseInt(payload.to_add);
    },
    deductFunds (state, payload) {

      // check to see if this is a valid transaction
      if ( !isNaN( payload.to_deduct ) && payload.to_deduct > 0 )
        state.amount -= parseInt(payload.to_deduct);

      if ( state.amount < 0 )
        state.amount -= 5;

    }
  }
})